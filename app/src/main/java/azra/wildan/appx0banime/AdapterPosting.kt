package azra.wildan.appx0banime

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_posting.*

class AdapterPosting(val dataPst : List<HashMap<String,String>>, val pstActivity: PostingActivity) :
    RecyclerView.Adapter<AdapterPosting.HolderDataPosting>(){
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterPosting.HolderDataPosting {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_posting,p0,false)
        return  HolderDataPosting(v)
    }

    override fun getItemCount(): Int {
        return dataPst.size
    }

    override fun onBindViewHolder(p0: AdapterPosting.HolderDataPosting, p1: Int) {
        val data = dataPst.get(p1)
        p0.txId.setText(data.get("id_karakter"))
        p0.txNama.setText(data.get("nama_karakter"))
        p0.txAsalAnime.setText(data.get("judul_anime"))
        p0.txSeiyuu.setText(data.get("seiyuu"))
        p0.txStudio.setText(data.get("studio"))
        if(p1.rem(2) == 0) p0.cLayout.setBackgroundColor(
            Color.rgb(230,245,240))
        else p0.cLayout.setBackgroundColor(Color.rgb(255,255,245))

        p0.cLayout.setOnClickListener({
            val pos = pstActivity.daftarAnime.indexOf(data.get("judul_anime"))
            pstActivity.spinner.setSelection(pos)
            pstActivity.edIdKarakter.setText(data.get("id_karakter"))
            pstActivity.edNama.setText(data.get("nama_karakter"))
            pstActivity.edSeiyuu.setText(data.get("seiyuu"))
            pstActivity.edStudio.setText(data.get("studio"))
            Picasso.get().load(data.get("url")).into(pstActivity.imgUpload)
        })

        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.photo)
    }

    class HolderDataPosting(v: View) : RecyclerView.ViewHolder(v){
        val txId = v.findViewById<TextView>(R.id.txId)
        val txNama = v.findViewById<TextView>(R.id.txNama)
        val txAsalAnime = v.findViewById<TextView>(R.id.txAsalAnime)
        val txSeiyuu = v.findViewById<TextView>(R.id.txSeiyuu)
        val txStudio = v.findViewById<TextView>(R.id.txStudio)
        val photo = v.findViewById<ImageView>(R.id.img)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.cLayout)
    }
}